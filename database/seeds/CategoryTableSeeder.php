<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [

                'name' => 'Starter Account',
                'percent' => '0',
                'color' => '1',
                'icon' => '0',
                'children' => [

                    ['name' => 'Starter Account'],


                     ]
            ],
             [

                'name' => 'Kruger Rand',
                'percent' => '500',
                'color' => '2',
                'icon' => '999',
                'children' => [

                    ['name' => 'Kruger Rand'],


                     ]
            ],
             [

                'name' => 'USDC Coin',
                'percent' => '1000',
                'color' => '3',
                'icon' => '1499',
                'children' => [

                    ['name' => 'USDC Coin'],


                     ]
            ],
            [

                'name' => 'Litecoin',
                'percent' => '1500',
                'color' => '4',
                'icon' => '1999',
                'children' => [

                    ['name' => 'Litecoin'],


                     ]
            ],

            [

                'name' => 'Ethereum',
                'percent' => '2000',
                'color' => '5',
                'icon' => '2499',
                'children' => [

                    ['name' => 'Ethereum'],


                     ]
            ],

             [

                'name' => 'Bitcoin',
                'percent' => '2500',
                'color' => '6',
                'icon' => '2999',
                'children' => [

                    ['name' => 'Bitcoin'],


                     ]
            ],
             [

                'name' => 'NFT',
                'percent' => '3000',
                'color' => '7',
                'icon' => '4999',
                'children' => [

                    ['name' => 'NFT'],


                     ]
            ],
             [

                'name' => 'Blockchain',
                'percent' => '5000',
                'color' => '8',
                'icon' => '10000',
                'children' => [

                    ['name' => 'Blockchain'],


                     ]
            ],

            
          


        ];

        foreach ($categories as $category) {
            \openjobs\Category::create($category);
        }
    }
}
