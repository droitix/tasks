 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Capitec Bank Deposit</h1>
                              <div class="text-muted font-heading text-small">Deposit</div>
                           </div>
                        </div>
                     </div>
                     <img src="/img/capitec.jpg" class="theme-filter" alt="launch"><br><br><br><br>
           <div class="card mb-5">

<div class="card-body sh-50 d-flex align-items-center justify-content-center">
<div class="text-center">

<div class="cta-1">BANK: CAPITEC</div>

<div class="cta-3 text-primary mb-4">ACCOUNT TYPE: CURRENT</div>
<div class="cta-3 text-primary mb-4"></div>
<div class="cta-3 text-primary mb-4">ACCOUNT NUMBER: 1517332034</div>

<div class="cta-3 text-danger mb-4">***Please make emmediate payment</div>

<div class="cta-3 text-primary mb-4">Whatsapp 0672310609 for Approval</div>

</div>
</div>
</div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection