 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Bonus Page</h1>
                              <div class="text-muted font-heading text-small">Bonuses</div>
                           </div>
                        </div>
                     </div>
                      <form action="{{ route('listings.store', [$area]) }}" method="post" class="buysell-form">
                     @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach

                     <h2 class="small-title">Bonus Balance {{ Auth::user()->area->unit }}{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-($withdrawn)}}.00</h2>
                     <input type="hidden" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-($withdrawn)}}">
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                        <input type="hidden" class="form-control" name="matched" id="value" value="0">
                                          <input type="hidden" class="form-control" name="value" id="value" value="1">
                                           <input type="hidden" class="form-control" name="current" id="value" value="1">
                                         <input type="hidden" class="form-control" name="recommit" id="period" value="1">
                                         <input type="hidden" class="form-control" name="period" id="period" value="1">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="area" value="2">
                            
                                        <div class="buysell-field form-action">
                                            <a type="submit" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#buy-coin">Not yet reached Minimum</a>
                                        </div><!-- .buysell-field -->
                                       
                                           {{ csrf_field() }}

                                    </form><!-- .buysell-form --><br>
                      <div class="table-responsive">
                              <table class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>Name</th>
                              
                                       <th>Contact</th>
                                       <th class="text-center">VIP Level</th>
                                       <th class="text-right">Email</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach ($referrals as $ref)
                                    <tr>
                                       <td>{{$ref->id}}<div class="logged-user-w">
                        <div class="logged-user-i"> <div class="avatar-w"><img alt="" src="img/avatar1.jpg"></div></div></div></td>
                                       <td>{{$ref->name}} {{$ref->surname}}</td>
                                        <td>{{$ref->phone_number}}</td>
                                        
                                        <td>{{$ref->accounttype}}</td>
                                       
                                       
                                       
                                       <td class="text-right">{{$ref->email}}</td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                              {{ $referrals->links() }}
                           </div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection