@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div class="container">
        <!-- Content Header (Page header) -->     
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="me-auto">
                    <h4 class="page-title">Proofs</h4>
                </div>
                
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                               
                                     @if ($files->count())
        @each ('resumes.partials._file_own', $files, 'file')

        @endif
 </section>
        <!-- /.content -->    
      </div>
  </div>
  <!-- /.content-wrapper -->



@endsection
