 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Check Your Submitted Orders</h1>
                              <div class="text-muted font-heading text-small">Panel</div>
                           </div>
                        </div>
                     </div>
                     <h2 class="small-title"></h2>
                     
       <div class="table-responsive">
                                        <table class="table table-centered table-nowrap">
                                        
                                        <thead>
                                            <tr>
                                                <th>Order Serial</th>
                                                    <th>Package</th>
                                                    <th>Commision Gained</th>
                                                     <th>Date</th>
                                                    <th class="text-right">Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                             @foreach ($listings as $listing)
                                                <tr>
                                                <td>{{$listing->id}}</td>
                                                    <td>
 {{$listing->category->name}}
                                                       
                                                    </td>
                                                    <td>R{{$listing->amount}}</td>
                                                    <td>{{$listing->created_at}}</td>
                                                    
                                                    
                                                                 <td class="text-right">
                                                        <div class="actions">
                                                            

                                                            <a href="#" class="btn btn-sm bg-danger"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete User"><i class="fe fe-trash"></i>Delete</a></li>
    <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>
                                                        </div>
                                                    </td>
                                            </tr>
                                              @endforeach
                                           
                                        </tbody>
                                        
                                    </table>
                                    {{ $listings->links() }}
      </div>
   </div>
</div>
    
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection