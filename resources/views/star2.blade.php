 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Standard Bank Deposit</h1>
                              <div class="text-muted font-heading text-small">Deposit</div>
                           </div>
                        </div>
                     </div>
                     <img src="/img/standard.jpg" class="theme-filter" alt="launch"><br><br><br><br>
           <div class="card mb-5">

<div class="card-body sh-50 d-flex align-items-center justify-content-center">
<div class="text-center">

<div class="cta-1">BANK: STANDARD BANK SOUTH AFRICA</div>
<div class="cta-3 text-primary mb-4">ACCOUNT: ALTCOIN TRADER PTY(LTD)</div>
<div class="cta-3 text-primary mb-4">ACCOUNT TYPE: CURRENT</div>
<div class="cta-3 text-primary mb-4">BRANCH CODE: 051001</div>
<div class="cta-3 text-primary mb-4">ACCOUNT NUMBER: 021276617</div>
<div class="cta-3 text-primary mb-4">REFERENCE: 278913058</div>
<div class="cta-3 text-danger mb-4">***Please make sure you put the exact reference above, else your funds will not reflect.</div>

<div class="cta-3 text-primary mb-4">Whatsapp 0672310609 for Approval</div>

</div>
</div>
</div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection