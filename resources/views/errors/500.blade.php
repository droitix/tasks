 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Error happened, please wait a minute while we resolve it</h1>
                              <div class="text-muted font-heading text-small"></div>
                           </div>
                        </div>
                     </div>
                     <h2 class="small-title"></h2>
                     <div class="card mb-5 col-lg-6">
                        <a href="{{url('/')}}" type="submit" class="btn btn-primary">Go back Home</a>
                        </div>
                     </div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection