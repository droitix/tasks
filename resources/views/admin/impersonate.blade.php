 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Impersonate</h1>
                              <div class="text-muted font-heading text-small">Impersonate</div>
                           </div>
                        </div>
                     </div>
                     <h2 class="small-title"></h2>
                     <div class="card mb-5 col-lg-6">
                        <div class="card-body ">
                          <form action="{{ route('admin.impersonate') }}" method="POST">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Email Address</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div><br>

                            <button type="submit" class="btn btn-success">Impersonate</button>
                            {{ csrf_field() }}
                        </form>
                        </div>
                     </div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection