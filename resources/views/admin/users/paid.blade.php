@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 


             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>Users</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Users</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                                      <div class="row">
                        <div class="col-xl-12 col-lg-12">
                         
                                <h3 class="title">
                                    
                                </h3>
                                <div class="table-responsive">
                                        <table class="table table-centered table-nowrap">
                                        
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Contact</th>
                                                    <th>Bank Account</th>
                                                    <th class="text-right">Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                             @foreach ($users as $user)
                                                <tr>
                                                    @if ($user->accounttype > 3)
                                                <td>{{$user->id}}</td>
                                                    <td>

                                                            {{$user->name}} {{$user->surname}}
                                                       
                                                    </td>
                                                    <td>{{$user->email}}</td>
                                                    <td>{{$user->phone_number}}</td>
                                                    <td>{{$user->bank}}  {{$user->account}}</td>
                                                                 <td class="text-right">
                                                        <div class="actions">
                                                            <a href="#" class="btn btn-sm bg-success-light mr-2">
                                                                <i class="fe fe-pencil"></i> Edit
                                                            </a>

                                                            <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $user->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete User"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.user.destroy', [$user->id])}}" method="post" id="listings-destroy-form-{{ $user->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                                        </div>
                                                    </td>
                                                    @else


                                                    @endif
                                            </tr>
                                              @endforeach
                                           
                                        </tbody>
                                        
                                    </table>
                                 
                                </div>
                            </div>
                        </div>
                    </div>     
                        
        


                   
                </div>
            </div>
            <!-- account end -->

@endsection
