@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <div class="element-box">
                           <h5 class="form-header">Paypal Payment</h5>
                           <div class="form-desc">Make sure you follow all the guidlines of the method you select</div>
                     
                  <div class="row">
                        <div class="col-sm-8">
                           <div class="element-wrapper compact pt-4">
                            <div class="col-lg-5 col-xxl-6">
                                 <div class="alert alert-warning borderless">
                                    <h5 class="alert-heading">Step 1</h5>
                                     <h6 class="alert-heading">Send the exact amount you want to deposit to the paypal account below</h6>
                                    <p>evolution5g@gmail.com</p>
                                    <h5 class="alert-heading">Step 2</h5>
                                     <h6 class="alert-heading">Click on Create Approval Order </h6>
                                    <div class="alert-btn"><a class="btn btn-white-gold" href="{{ route('listings.create', [$area]) }}"><i class="os-icon os-icon-ui-92"></i><span>Create Approval Order</span></a></div>
                                 </div>
                              </div>  
                            
                           </div>
                           <div class="row">
                              <div class="col-12 col-xxl-8">
                                 <div class="element-wrapper compact pt-4">
                                    <div class="element-actions d-none d-sm-block">
                                      
                                    </div>
                                   
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                       
                     </div>


                           </div>
                        </div>
                     </div>
                     
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="floated-chat-btn"><i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span></div>
                     <div class="floated-chat-w">
                        <div class="floated-chat-i">
                           <div class="chat-close"><i class="os-icon os-icon-close"></i></div>
                           <div class="chat-head">
                              <div class="user-w with-status status-green">
                                 <div class="user-avatar-w">
                                    <div class="user-avatar"><img alt="" src="img/avatar1.jpg"></div>
                                 </div>
                                 <div class="user-name">
                                    <h6 class="user-title">John Mayers</h6>
                                    <div class="user-role">Account Manager</div>
                                 </div>
                              </div>
                           </div>
                           <div class="chat-messages">
                              <div class="message">
                                 <div class="message-content">Hi, how can I help you?</div>
                              </div>
                              <div class="date-break">Mon 10:20am</div>
                              <div class="message">
                                 <div class="message-content">Hi, my name is Mike, I will be happy to assist you</div>
                              </div>
                              <div class="message self">
                                 <div class="message-content">Hi, I tried ordering this product and it keeps showing me error code.</div>
                              </div>
                           </div>
                           <div class="chat-controls">
                              <input class="message-input" placeholder="Type your message here...">
                              <div class="chat-extra"><a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
@endsection
