@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

 ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">

                 <div class="row">
                    <div class="col-md-12">
                         <div class="card mb-5">
                            <div class="card-body">
                               <h4>{{$comment->listing->user->name}} {{$comment->listing->user->surname}}</h4>
                               <h4>{{$comment->listing->user->bank}} | {{$comment->listing->user->account}}</h4>
                                <h4>Contact Number: {{$comment->listing->user->phone}}</h4>
                            </div>
                        </div>
                        </div>
                        <!-- end row -->




                    </div> <!-- end container-fluid -->

                </div> <!-- end content -->


            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->

@endsection
