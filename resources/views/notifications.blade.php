@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>Notifications</h1>
                                
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                   <div class="col-xl-8 col-lg-10 col-md-7 col-sm-9">
                    @foreach (auth()->user()->unreadNotifications as $notification)
                        @if(!empty($notification->data['notification_message']))
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                {{$notification->data['notification_message']}}
                                
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" data-notifications-id="{{$notification->id}}" onclick="markAsRead(event)">&times;</span>
                                </button>
                            </div>
                        @endif
                    @endforeach
                </div>


                   
                </div>
            </div>
            <!-- account end -->

@endsection
<script>
    function markAsRead(e) {
        $.ajax({
            url: `notifications/${e.target.dataset.notificationsId}/mark-as-read`,
            type: "post",
            data: {'_token': "{{csrf_token()}}"},
            success: function (response) {/* */},
            error: function(jqXHR, textStatus, errorThrown) { /* console.log(textStatus, errorThrown); */ }
        });
    }
</script>