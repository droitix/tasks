 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Deposit</h1>
                              <div class="text-muted font-heading text-small">Account</div>
                           </div>
                        </div>
                     </div>
                      
                   @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               
 

                         @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach


                      <h2 class="small-title">Account Balance R{{$sum}}</h2>
                     <div class="card mb-5 col-lg-6">
                        <div class="card-body ">
                           <form class="form" method="post" action="{{ route('listings.store', [$area]) }}">
                                    <h5 class="element-box-header">Deposit</h5>
                                    <div class="row">
                               <div class="mb-3 filled w-100 d-flex flex-column">
                                 <i data-cs-icon="dollar"></i>
                                 <input class="form-control" placeholder="Amount" name="amount">
                              </div>
                          
                            <input type="hidden" class="form-control" name="name" id="value" value="withdraw">
                            <input type="hidden" class="form-control" name="matched" id="value" value="0">
                                         <input type="hidden" class="form-control" name="descrep" id="value" value="descrep">
                                       <input type="hidden" class="form-control" name="piclink" id="value" value="1">
                                       <input type="hidden" class="form-control" name="extamount" id="value" value="500">
                                       <input type="hidden" class="form-control" name="serial" id="value" value="11111111">
                                       <input type="hidden" class="form-control" name="vip" id="value" value="1">
                                       
                                       <input type="hidden" class="form-control" name="value" id="value" value="0.045">
                                       <input type="hidden" class="form-control" name="period" id="period" value="28">
                                       <input type="hidden" class="form-control" name="current" id="current" value="0">
                                       <input type="hidden" class="form-control" name="recommit" id="period" value="0">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="category" value="2">
                                    </div>
                                    <div class="form-buttons-w text-right compact"><button class="btn btn-primary" type="submit" ><span>Deposit</span><i class="os-icon os-icon-grid-18"></i></button></div>
                                    {{ csrf_field() }} 
                                 </form>
                          
                        </div>
                     </div>

                     
                  </div>
               </div>
            </div>
         </main>

 
@endsection