<h2 class="small-title">Security Logs</h2>
<div class="card">
<div class="card-body">
<div class="mb-2 bg-transparent no-shadow d-none d-md-block g-0 sh-3">
   <div class="row g-0 h-100 align-content-center">
      <div class="col-12 col-md-5 d-flex align-items-center mb-2 mb-md-0 text-muted text-small">TITLE</div>
      <div class="col-6 col-md-3 d-flex align-items-center text-alternate text-medium text-muted text-small">IP</div>
      <div class="col-6 col-md-2 d-flex align-items-center text-alternate text-medium text-muted text-small">TIME</div>
      <div class="col-6 col-md-2 d-flex align-items-center text-alternate text-medium justify-content-end text-muted text-small">ACTION</div>
   </div>
</div>
<div class="mb-5 border-last-none">
   <div class="h-100 sh-md-4 border-bottom border-separator-light pb-3 mb-3">
      <div class="row g-0 h-100 align-content-center">
         <div class="col-11 col-md-5 d-flex flex-column justify-content-center mb-2 mb-md-0 order-0">
            <div class="text-muted text-small d-md-none">Title</div>
            <div class="text-alternate">Password Change</div>
         </div>
         <div class="col-12 col-md-3 d-flex flex-column justify-content-center mb-2 mb-md-0 order-2">
            <div class="text-muted text-small d-md-none">Ip</div>
            <div class="text-alternate">241.157.15.24</div>
         </div>
         <div class="col-12 col-md-3 d-flex flex-column justify-content-center mb-2 mb-md-0 order-3">
            <div class="text-muted text-small d-md-none">Time</div>
            <div class="text-alternate">21.03.2021 - 16:10</div>
         </div>
         <div class="col-1 d-flex flex-column justify-content-center align-items-md-end mb-2 mb-md-0 order-1 order-md-4">
            <a class="link" href="#">
            <i data-acorn-icon="more-horizontal"></i>
            </a>
         </div>
      </div>
   </div>
   
      </div>
   </div>
</div>