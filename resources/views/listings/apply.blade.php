@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div class="container">
        <!-- Content Header (Page header) -->     
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="me-auto">
                    <h4 class="page-title">Withdraw from your account</h4>
                </div>
                
            </div>
        </div>

       <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-start mb-30">
                        <a href="{{url('dashboard')}}" class="me-10 btn btn-success">Dashboard</a>
                        <a href="{{ route('listings.published.index', [$area]) }}" class="me-10 btn btn-primary">Deposit</a>
                    </div>
                </div>
                <div class="col-4">
                   
                              <form action="{{ route('comments.store', [$listing->id]) }}" method="post" class="buysell-form">
                                                                          @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)

                           @if ($loop->last)

                           @endif



                                  @endforeach
                                  @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                   @php($percentage = $listing->value)
                                   @php($multiplier = ($listing->amount-$listing->current))

                                        <div class="buysell-field form-group">
                                            <div class="form-label-group">
                                                <label class="form-label" for="buysell-amount">{{ Auth::user()->area->unit }} {{($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount)-($listing->scrap)}} Available for withdrawal </label>
                                            </div>
                                            <div class="form-control-group">
                                                <input type="text" class="form-control form-control-lg form-control-number" id="buysell-amount" name="split" placeholder="Amount">
                                                <div class="form-dropdown">
                                                   
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-indicator-caret" data-toggle="dropdown" data-offset="0,2"></a>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-note-group">
                                                <span class="buysell-min form-note-alt"></span>
                                               
                                            </div>
                                        </div><!-- .buysell-field -->
                                           <input type="hidden" class="form-control" name="body" id="body" value="1">
                                            <input type="hidden" class="form-control" name="category_id" id="body" value="2">
                            
                                        <div class="buysell-field form-action">
                                            <button type = "submit" class="btn btn-lg btn-block btn-primary"  >Withdraw Now</button>
                                        </div><!-- .buysell-field -->
                                        <div class="form-note text-base text-center"><a href="#"></a>.</div>
                                           {{ csrf_field() }}

                                    </form><!-- .buysell-form -->
                  
                    </div>
                   
                </div>
            </div>
        </section>
        <!-- /.content -->
      </div>
  </div>
  <!-- /.content-wrapper -->


@endsection
