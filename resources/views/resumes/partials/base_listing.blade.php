<div class="col-lg-12 mt30">
    <a href="{{ route('listings.show', [$area, $listing]) }}">
                            <div class="fj_post style2 jlv5">
                                <div class="details">
                                    <h5 class="job_chedule text-thm">{{$listing->position}}</h5>
                                    <div class="thumb fn-smd">
                                        <img class="img-fluid" src="/images/partners/1.jpg" alt="1.jpg">
                                    </div>
                                    <h4>{{$listing->jobtitle}}</h4>
                                     <p>Posted {{$listing->created_at->diffForHumans()}} by <a class="text-thm" href="#">{{$listing->companyname}}</a></p>
                                    <ul class="featurej_post">
                                        <li class="list-inline-item"><span class="flaticon-location-pin"></span> <a href="#">{{ $listing->area->parent->name }}, {{ $listing->area->name }}</a></li>
                                        <li class="list-inline-item"><span class="flaticon-price pl20"></span> <a href="#">{{$listing->salary}}</a></li>
                                    </ul>
                                </div>
                                <ul class="pjlv5">
                                    <li class="list-inline-item">
                                        <a class="favorit" href="#"><span class="flaticon-favorites"></span></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="btn btn-transparent" href="#">Browse Job</a>
                                    </li>
                                </ul>
                            </div>
                             </a>
                        </div>
