 
@extends('layouts.regapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
 
 <main>
            <div class="container">
               <div class="row">
                 
                 @include('layouts.partials.sidebar') 

                 <div class="col">
                     <div class="page-title-container mb-3">
                        <div class="row">
                           <div class="col mb-2">
                              <h1 class="mb-2 pb-0 display-4" id="title">Change Your Password</h1>
                              <div class="text-muted font-heading text-small">Change</div>
                           </div>
                        </div>
                     </div>
                     <h2 class="small-title"></h2>
                     <div class="card mb-5 col-lg-6">
                        <div class="card-body ">
                           <form action="{{ route('password.update') }}" method="post" class="d-flex flex-column mb-4">
                                      @csrf
                             <input type="hidden" name="token" value="{{ $token }}">

                              <div class="mb-3 filled w-100 d-flex flex-column">
                                 <i data-cs-icon="email"></i>
                                 <input class="form-control" placeholder="Email" name="email" >
                              </div>
                             

                              <div class="mb-3 filled w-100 d-flex flex-column">
                                 <i data-cs-icon="tag"></i>
                                 <input class="form-control" type="password" name="password" placeholder="New Password" >
                              </div>


                              <div class="mb-3 filled w-100 d-flex flex-column">
                                 <i data-cs-icon="tag"></i>
                                 <input class="form-control" type="password" name="password_confirmation" required placeholder="Confirm Password" >
                              </div>

                             

                              <button type="submit" class="btn btn-primary">Create</button>
                              
                           </form>
                           
                        </div>
                     </div>
                    
                  </div>
               </div>
            </div>
         </main>

 
@endsection