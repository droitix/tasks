<div class="content-panel">
                     <div class="content-panel-close"><i class="os-icon os-icon-close"></i></div>
                     <div class="element-wrapper">
                        <h6 class="element-header">Your Manager</h6>
                        <div class="element-box-tp">
                          <div class="profile-tile">
                              <a class="profile-tile-box" href="users_profile_small.html">
                                 <div class="pt-avatar-w"><img alt="" src="img/avatar2.jpg"></div>
                                 <div class="pt-user-name">{{Auth::user()->referrer->name}} {{Auth::user()->referrer->surname}}</div>
                              </a>
                              <div class="profile-tile-meta">
                                 <ul>
                                    <li>Contact:<strong>{{Auth::user()->referrer->phone_number}}</strong></li>
                                    <li>Country:<strong><a href="#">{{Auth::user()->referrer->area->name}}</a></strong></li>
                                    <li>Affiliates:<strong>{{Auth::user()->referrer->referrals()->count()}}</strong></li>
                                 </ul>
                                 <div class="pt-btn"><a class="btn btn-success btn-sm" href="#">{{Auth::user()->referrer->accounttype}}</a></div>
                              </div>
                           </div>
                        </div>
                     </div>
                   
                    
                  </div>