 <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
      <title>Crypto Clicks | Promote Crypto</title>
      <meta name="description" content="Crypto Clicks">
      
      <link rel="icon" type="image/png" href="/img/favicon/favicon.png" >
      <link rel="preconnect" href="../fonts.gstatic.com/index.html">
      <link href="../fonts.googleapis.com/css27619.css?family=Nunito+Sans:wght@300;400;600&amp;display=swap" rel="stylesheet">
      <link href="../fonts.googleapis.com/css2ac11.css?family=Montserrat:wght@300;400;700&amp;display=swap" rel="stylesheet">
      <link rel="stylesheet" href="/font/CS-Interface/style.css">
      <link rel="stylesheet" href="/css/vendor/bootstrap.min.css">
      <link rel="stylesheet" href="/css/vendor/OverlayScrollbars.min.css">
      <link rel="stylesheet" href="/css/styles.css">
      <link rel="stylesheet" href="/css/main.css">
      <script src="/js/base/loader.js"></script>
      <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>