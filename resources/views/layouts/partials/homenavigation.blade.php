 <header class="header d-none d-lg-block sticky-on">
         <div id="sticky-placeholder"></div>
         <div id="topbar-wrap" class="header-top header-top__border_bottom header-top__padding container-custom">
            <div class="container-fluid">
               <div class="row align-items-center">
                  <div class="col-lg-9">
                     <div class="header-top__info">
                        <div class="header-top__info_item header-top__info_time">
                           <div class="header-top__info_icon"><i class="flaticon flaticon-clock"></i></div>
                           <div class="header-top__info_text">
                              <p class="header-top__info_text--small">Sunday - Friday:</p>
                              <span class="header-top__info_text--big">9am - 8pm</span>
                           </div>
                        </div>
                        <div class="header-top__info_item header-top__info_phone">
                           <div class="header-top__info_icon"><i class="flaticon flaticon-phone-call"></i></div>
                           <div class="header-top__info_text">
                              <p class="header-top__info_text--small">Call for help:</p>
                              <span class="header-top__info_text--big">(+123) 5462 3257</span>
                           </div>
                        </div>
                        <div class="header-top__info_item header-top__info_email">
                           <div class="header-top__info_icon"><i class="flaticon flaticon-envelope"></i></div>
                           <div class="header-top__info_text">
                              <p class="header-top__info_text--small">Mail to us</p>
                              <span class="header-top__info_text--big">envato@gmail.com</span>
                           </div>
                        </div>
                        <div class="header-top__info_item header-top__info_address">
                           <div class="header-top__info_icon"><i class="flaticon flaticon-location"></i></div>
                           <div class="header-top__info_text">
                              <p class="header-top__info_text--small">Our Address:</p>
                              <span class="header-top__info_text--big">380 Albert St, Melbourne</span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 d-none d-lg-block">
                     <div class="header-top__social">
                        <ul>
                           <li class="header-top__social_list"><a class="header-top__social_list--link facebook" href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                           <li class="header-top__social_list"><a class="header-top__social_list--link twitter" href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                           <li class="header-top__social_list"><a class="header-top__social_list--link instagram" href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                           <li class="header-top__social_list"><a class="header-top__social_list--link linkedin" href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a></li>
                           <li class="header-top__social_list"><a class="header-top__social_list--link pinterest" href="javascript:void(0);"><i class="fab fa-pinterest-p"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="navbar-wrap" class="header-bottom container-custom navbar-wrap white-bg">
            <div class="container-fluid">
               <div class="header-bottom__row">
                  <div class="header-bottom__col">
                     <div class="logo"><a href="{{url('/')}}" class="logo__link"><img src="assets/images/logodarker.png" height="150" width="200" alt="IT Solution Techkit"></a></div>
                  </div>
                  <div class="header-bottom__col">
                     <div class="main-menu">
                        <nav class="main-menu__nav">
                           <ul>
                              <li class="main-menu__nav_sub list">
                                 <a class="animation" href="index.html">Home</a>
                                 <ul class="main-menu__dropdown">
                                    <li><a href="index.html">Main Home</a></li>
                                    <li><a href="software-innovation.html">Software Company</a></li>
                                    <li><a href="it-agency.html">IT Agency</a></li>
                                    <li><a href="vertical-slider.html">Vertical Slider</a></li>
                                    <li><a href="it-modern.html">IT Modern</a></li>
                                    <li><a href="it-startup.html">IT Startup</a></li>
                                 </ul>
                              </li>
                              <li class="main-menu__nav_sub list">
                                 <a class="animation" href="about-us.html">Company</a>
                                 <ul class="main-menu__dropdown">
                                    <li><a href="about-us.html">About Us</a></li>
                                    <li><a href="why-chose-us.html">Why Chose Us</a></li>
                                    <li><a href="faq.html">Help & FAQ'S</a></li>
                                    <li><a href="history.html">Our History</a></li>
                                    <li><a href="careers.html">Careers</a></li>
                                 </ul>
                              </li>
                              <li class="main-menu__nav_sub list">
                                 <a class="animation" href="it-solution.html">It Solutions</a>
                                 <ul class="main-menu__dropdown">
                                    <li><a href="it-solution.html">It Solutions</a></li>
                                    <li><a href="it-services.html">It Services</a></li>
                                    <li><a href="industries-services.html">Industries Services</a></li>
                                    <li><a href="services-details.html">Services Details 01</a></li>
                                    <li><a href="services-details-02.html">Services Details 02</a></li>
                                    <li><a href="services-details-03.html">Services Details 03</a></li>
                                 </ul>
                              </li>
                              <li class="main-menu__nav_sub list">
                                 <a class="animation" href="index.html">Pages</a>
                                 <ul class="main-menu__dropdown">
                                    <li><a href="team.html">Our Team</a></li>
                                    <li><a href="price.html">Pricing Plans</a></li>
                                    <li><a href="404.html">404 Page</a></li>
                                    <li><a href="case-01.html">Case Studies 01</a></li>
                                    <li><a href="case-02.html">Case Studies 02</a></li>
                                    <li><a href="case-03.html">Case Studies 03</a></li>
                                    <li><a href="case-details.html">Case Details</a></li>
                                 </ul>
                              </li>
                              <li class="main-menu__nav_sub list">
                                 <a class="animation" href="blog-details.html">Blogs</a>
                                 <ul class="main-menu__dropdown">
                                    <li><a href="grid-layout.html">Grid Layout</a></li>
                                    <li><a href="list-layout.html">List Layout</a></li>
                                    <li><a href="blog-details-no-sidebar.html">No Sidebar</a></li>
                                    <li><a href="blog-details.html">Blog Details</a></li>
                                 </ul>
                              </li>
                              <li class="list"><a class="animation" href="contact.html">Contact</a></li>
                           </ul>
                        </nav>
                     </div>
                  </div>
                  <form class="header-bottom__col d-none d-lg-block">
                     <div class="header-search"><input placeholder="Search" type="text" class="header-search__input"> <button class="header-search__button"><i class="fas fa-search"></i></button></div>
                  </form>
               </div>
            </div>
         </div>
      </header>
         <div class="rt-header-menu mean-container position-relative" id="meanmenu">
         <div class="mean-bar">
            <a href="{{url('/')}}"><img class="logo-small" alt="Techkit" src="assets/images/logodarker.png"></a>
            <form class="header-bottom__col">
               <div class="header-search"><input placeholder="Search" type="text" class="header-search__input"> <button class="header-search__button"><i class="fas fa-search"></i></button></div>
            </form>
            <span class="sidebarBtn"><span class="fa fa-bars"></span></span>
         </div>
         <div class="rt-slide-nav">
            <div class="offscreen-navigation">
               <nav class="menu-main-primary-container">
                  <ul class="menu">
                     <li class="list menu-item-parent menu-item-has-children">
                        <a class="animation" href="index.html">Home</a>
                        <ul class="main-menu__dropdown sub-menu">
                           <li><a href="index.html">Main Home</a></li>
                           <li><a href="software-innovation.html">Software Company</a></li>
                           <li><a href="it-agency.html">IT Agency</a></li>
                           <li><a href="vertical-slider.html">Vertical Slider</a></li>
                        </ul>
                     </li>
                     <li class="list menu-item-parent menu-item-has-children">
                        <a class="animation" href="about-us.html">Company</a>
                        <ul class="main-menu__dropdown sub-menu">
                           <li><a href="about-us.html">About Us</a></li>
                           <li><a href="why-chose-us.html">Why Chose Us</a></li>
                           <li><a href="faq.html">Help & FAQ'S</a></li>
                           <li><a href="history.html">Our History</a></li>
                           <li><a href="careers.html">Careers</a></li>
                        </ul>
                     </li>
                     <li class="list menu-item-parent menu-item-has-children">
                        <a class="animation" href="it-solution.html">It Solutions</a>
                        <ul class="main-menu__dropdown sub-menu">
                           <li><a href="it-solution.html">It Solutions</a></li>
                           <li><a href="it-services.html">It Services</a></li>
                           <li><a href="industries-services.html">Industries Services</a></li>
                           <li><a href="services-details.html">Services Details 01</a></li>
                           <li><a href="services-details-02.html">Services Details 02</a></li>
                           <li><a href="services-details-03.html">Services Details 03</a></li>
                        </ul>
                     </li>
                     <li class="list menu-item-parent menu-item-has-children">
                        <a class="animation" href="index.html">Pages</a>
                        <ul class="main-menu__dropdown sub-menu">
                           <li><a href="team.html">Our Team</a></li>
                           <li><a href="price.html">Pricing Plans</a></li>
                           <li><a href="404.html">404 Page</a></li>
                           <li><a href="case-01.html">Case Studies 01</a></li>
                           <li><a href="case-02.html">Case Studies 02</a></li>
                           <li><a href="case-03.html">Case Studies 03</a></li>
                           <li><a href="case-details.html">Case Details</a></li>
                        </ul>
                     </li>
                     <li class="list menu-item-parent menu-item-has-children">
                        <a class="animation" href="blog-details.html">Blogs</a>
                        <ul class="main-menu__dropdown sub-menu">
                           <li><a href="grid-layout.html">Grid Layout</a></li>
                           <li><a href="list-layout.html">List Layout</a></li>
                           <li><a href="blog-details-no-sidebar.html">No Sidebar</a></li>
                           <li><a href="blog-details.html">Blog Details</a></li>
                        </ul>
                     </li>
                     <li class="list menu-item-parent"><a class="animation" href="contact.html">Contact</a></li>
                  </ul>
               </nav>
            </div>
         </div>
      </div>