<?php

namespace openjobs\Http\Controllers\Resume;

use Mail;
use openjobs\{Area, Resume};
use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use openjobs\Http\Requests\StoreResumeShareFormRequest;
use openjobs\Mail\ResumeShared;

class ResumeShareController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Area $area, Resume $resume)
    {
        return view('resumes.share.index', compact('resume'));
    }

    public function store(StoreResumeShareFormRequest $request, Area $area, Resume $resume)
    {
        collect(array_filter($request->emails))->each(function ($email) use ($resume, $request) {
            Mail::to($email)->queue(
                new ResumeShared($resume, $request->user(), $request->message)
            );
        });

        return redirect()->route('resumes.show', [$area, $resume])->withSuccess('Resume shared successfully.');
    }
}
