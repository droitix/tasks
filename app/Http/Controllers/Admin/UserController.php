<?php

namespace openjobs\Http\Controllers\Admin;

use Illuminate\Http\Request;
use openjobs\User;
use openjobs\Http\Controllers\Controller;


class UserController extends Controller
{
    public function index()
    {
        $registeredusers = User::count();
        $users = \openjobs\User::orderBy('updated_at', 'desc')->paginate(2000);
        return view('admin.users.index')->with(compact('users','registeredusers'));

    }

    public function paid()

    {


                $users = \openjobs\User::orderBy('updated_at', 'desc')->paginate(2000);
        return view('admin.users.paid')->with(compact('users'));
    }

     public function destroyUser($id)
    {
        User::where('id', $id)->delete();
        session()->flash('message', 'User deleted!');
        notify()->success('User Deleted');
        return redirect()->back();
    }
}
