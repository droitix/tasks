<?php

namespace openjobs\Http\Controllers\Admin;


use Illuminate\Http\Request;
use openjobs\Comment;
use openjobs\User;
use openjobs\Http\Controllers\Controller;

class CommentController extends Controller
{


     public function destroyComment($id)
    {
        Comment::where('id', $id)->delete();
        session()->flash('message', 'Comment deleted!');
         notify()->success('comment Deleted');
        return redirect()->back();
    }
}
