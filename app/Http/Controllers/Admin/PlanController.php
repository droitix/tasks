<?php

namespace openjobs\Http\Controllers\Admin;

use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;

class PlanController extends Controller
{
  public function index()
    {
        return view('admin.plans.index');
    }
}
